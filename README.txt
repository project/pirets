
PIRETS
by Garrett Albright, Precision Intermedia

PIRETS is a module for the Drupal content management system which connects to
real estate listing providers using the RETS (Real Estate Transaction Standard)
protocol and imports listings as Drupal nodes. PIRETS is the first step to
building a truly great real estate listing site with Drupal.

For more information and installation and configuration documentation, see the
PIRETS web site at:
http://pirets.info/

This module's project page:
http://drupal.org/project/pirets

Garrett Albright's user page:
http://drupal.org/user/191212