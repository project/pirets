<?php

/**
 * @file
 * Various batch operations related to listing fetching, updating and deletion.
 */

module_load_include('inc', 'pirets', 'pirets.connect');

/**
 * Set up the batch job and initialize it.
 *
 * @param $class
 *   The property class to update.
 * @param $progressive
 *   Whether this should be run as a progressive batch or not. This needs to be
 *   FALSE when we run updates via cron in particular.
 * @see pirets_cron()
 */
function pirets_class_update_batch($class, $progressive = TRUE) {
  // Are we not currently fetching/processing listings?
  if (lock_acquire('pirets_fetch')) {
    $classes = pirets_classes_fetch();
    if (isset($classes[$class]) && $classes[$class]['enabled']) {
      batch_set(array(
        'title' => t('Updating properties in class @class (@visible)', array('@class' => $class, '@visible' => $classes[$class]['visible'])),
        'operations' => array(
          array('pirets_class_update_batch_fetch', array($class)),
          array('pirets_class_update_batch_update', array()),
          array('pirets_class_update_batch_del', array()),
        ),
        'finished' => 'pirets_class_update_batch_finished',
        'file' => drupal_get_path('module', 'pirets') . '/pirets.batch.inc',
      ));
      // http://drupal.org/node/638712
      $batch =& batch_get();
      $batch['progressive'] = $progressive;
      batch_process('admin/settings/pirets/update');
    }
    else {
      drupal_not_found();
      return;
    }
  }
  else {
    if (variable_get('cron_semaphore', FALSE)) {
      watchdog('pirets', 'A PIRETS listing fetch process was not started because it appears that PIRETS is already currently fetching listings (the PIRETS listings fetch semaphore is in place). Try again later.', array(), WATCHDOG_WARNING);
    }
    else {
      drupal_set_message(t('A PIRETS listing fetch process was not started because it appears that PIRETS is already currently fetching listings (the PIRETS listings fetch semaphore is in place). Try again later.'), 'warning');
      drupal_goto('admin/settings/pirets/update');
    }
  }
}

/**
 * Set up a batch job to update listings in all active classes.
 */
function pirets_all_update_batch() {
  if (lock_acquire('pirets_fetch')) {
    $batch = array(
      'title' => t('Updating properties in all active classes'),
      'operations' => array(),
      'finished' => 'pirets_all_update_batch_finished',
      'file' => drupal_get_path('module', 'pirets') . '/pirets.batch.inc',
    );
    foreach (pirets_classes_fetch() as $short => $class) {
      if ($class['enabled']) {
        $batch['operations'][] = array('pirets_class_update_batch_fetch', array($short));
        $batch['operations'][] = array('pirets_class_update_batch_update', array());
        $batch['operations'][] = array('pirets_class_update_batch_del', array());
      }
    }
    batch_set($batch);
    batch_process('admin/settings/pirets/update');
  }
  else {
    if (variable_get('cron_semaphore', FALSE)) {
      watchdog('pirets', 'A PIRETS listing fetch process was not started because it appears that PIRETS is already currently fetching listings (the PIRETS listings fetch semaphore is in place). Try again later.', array(), WATCHDOG_WARNING);
    }
    else {
      drupal_set_message(t('A PIRETS listing fetch process was not started because it appears that PIRETS is already currently fetching listings (the PIRETS listings fetch semaphore is in place). Try again later.'), 'warning');
      drupal_goto('admin/settings/pirets/update');
    }
  }
}

/**
 * Fetch the listings from the RETS server. Batch callback.
 */
function pirets_class_update_batch_fetch($class, &$context) {
  // Setup if this is the first time running
  if (!count($context['sandbox'])) {
    $context['sandbox']['offset'] = 0;
    $context['sandbox']['props'] = array();
    $context['results']['ask_fields'] = array();
    $context['results']['class'] = $class;
    $context['results']['error'] = FALSE;
    if (!isset($context['results']['counts'])) {
      $context['results']['counts'] = array();
    }
    $context['results']['counts'][$class] = array(
      'checked' => 0,
      'new' => 0,
      'update' => 0,
      'del' => 0,
      'keep' => 0,
    );

    // Build a list of fields we're asking for.
    $context['results']['fields'] = pirets_fields_active_fetch();
    // foreach ($fields as &$field) {
    // This effs up the last element of $fields for later iteration through it,
    // so let's do it ghetto-style.
    foreach (array_keys($context['results']['fields']) as $field) {
      $context['results']['fields'][$field]['classes'] = explode(',', $context['results']['fields'][$field]['classes']);
      if (in_array($class, $context['results']['fields'][$field]['classes'])) {
        $context['results']['ask_fields'][] = $field;
        if ($context['results']['fields'][$field]['correlation'] === 'mls_id') {
          $context['results']['mls_field'] = &$context['results']['fields'][$field];
        }
        elseif ($context['results']['fields'][$field]['correlation'] === 'sale_status') {
          $status_field = &$context['results']['fields'][$field];
        }
        elseif ($context['results']['fields'][$field]['correlation'] === 'price') {
          $price_field = &$context['results']['fields'][$field];
        }
      }
    }

    // Build the query.
    $statuses = variable_get('pirets_limit_sstatuses', array());
    // Rappatoni wants us to use the "or" operator, even if there's just one
    // status. Other servers seem okay with this.
    $statuses_q = '|' . implode(',', $statuses);
    $context['sandbox']['limit'] = variable_get('pirets_chunk', 2000);
    if ($context['sandbox']['limit'] == 0) {
      $context['sandbox']['limit'] = 'NONE';
    }
    $context['results']['chunks'] = 0;
    $context['sandbox']['query'] = array("{$status_field['system_name']}={$statuses_q}");
    if (isset($price_field)) {
      $context['sandbox']['query'][] = "{$price_field['system_name']}=0+";
    }
  }

  // Reacquire the semaphore.
  lock_acquire('pirets_fetch');
  $response = pirets_query_search($class, $context['sandbox']['query'], $context['results']['ask_fields'], $context['sandbox']['limit'], $context['sandbox']['offset']);
  if ($response->response_code === 200) {
    $chunk = $response->data;
    cache_set('pirets_chunk_' . $context['results']['chunks']++, $chunk);
    $context['sandbox']['offset'] += count($chunk);
  }
  else {
    $context['results']['error'] = TRUE;
    $context['finished'] = 1;
    return;
  }

  $context['finished'] = strpos($response->data_raw, '<MAXROWS') === FALSE;
  $context['message'] = t('@count listings fetched from RETS server for class @class.', array('@count' => $context['sandbox']['offset'], '@class' => $class));
  $context['results']['counts'][$context['results']['class']]['total'] = $context['sandbox']['offset'];
}

/**
 * Insert new listings; update current ones. Batch callback.
 */
function pirets_class_update_batch_update(&$context) {
  if (!count($context['sandbox'])) {
    if ($context['results']['error']) {
      $finished = 1;
      return;
    }
    // Load the info for the properties which already belong to
    // this class.
    $context['results']['db_props'] = array();
    $rez = db_query("SELECT nid, pirets_crc32_value, {$context['results']['mls_field']['cck']}_value AS mls FROM {content_type_pirets_prop} WHERE pirets_class_value = \"%s\"", $context['results']['class']);
    while ($prop = db_fetch_array($rez)) {
      $prop = array_map('intval', $prop);
      $context['results']['db_props'][$prop['mls']] = $prop;
    }
    $context['results']['in_rets'] = array();
    if ($context['results']['counts'][$context['results']['class']]['total'] === 0) {
      // We don't have any to update, but we might have some to delete…
      return;
    }
    $context['sandbox']['chunk_idx'] = 0;
    $context['sandbox']['in_rets'] = array();
/*     $context['sandbox']['stat_cache'] = array(); */
    $context['sandbox']['index'] = 0;
    $context['sandbox']['img_dir'] = file_directory_path() . '/pirets_img/';
    if (variable_get('pirets_use_img', FALSE)) {
      $context['sandbox']['placeholder'] = array(
        // Most of these fields are probably unnecessary…?
        'title' => t('PIRETS property image placeholder'),
        'filesize' => 34,
        'mimetype' => 'image/jpeg',
        'description' => '',
        'list' => TRUE,
        'data' => array(
          'alt' => t('PIRETS property image placeholder'),
          'title' => t('PIRETS property image placeholder'),
        ),
      );
      $context['sandbox']['file'] = new stdClass();
      $context['sandbox']['file']->filemime = 'image/jpeg';
      $context['sandbox']['file']->filesize = 0 ;
      $context['sandbox']['file']->uid = 0;
      $context['sandbox']['file']->status = FILE_STATUS_PERMANENT;
      $context['sandbox']['file']->timestamp = time();
      $context['sandbox']['file']->filepath = drupal_get_path('module', 'pirets') . '/img/pirets_blank.jpeg';
    }
    else {
      $context['sandbox']['file'] = FALSE;
    }
  }

  // Reacquire the semaphore.
  lock_acquire('pirets_fetch');
  // We're going to define some pointers because the code below gets *really*
  // ugly when we use the full array names.
  $_index = &$context['sandbox']['index'];
  $_counts = &$context['results']['counts'][$context['results']['class']];
  $_mls_field = &$context['results']['mls_field'];
  $_in_rets = &$context['results']['in_rets'];
  $_db_props = &$context['results']['db_props'];
  $_fields = &$context['results']['fields'];
  $_file = &$context['sandbox']['file'];

  $img_dir = file_directory_path() . '/pirets_img';
  $chunk_name = 'pirets_chunk_' . $context['sandbox']['chunk_idx'];
  $_props = cache_get($chunk_name);
  $chunk_count = count($_props->data);
  $context['finished'] = 0;

  // Batches of 50
  for ($x = 0; $x < 50 && $_index < $chunk_count && $context['finished'] < 1; $x++) {
    $prop = &$_props->data[$_index];
    $_in_rets[] = $prop[$_mls_field['system_name']];
    // If this property is not currently in the database OR its hash has changed…
    if (!isset($_db_props[$prop[$_mls_field['system_name']]]) || $_db_props[$prop[$_mls_field['system_name']]]['pirets_crc32_value'] != intval($prop['crc32'])) {
      if (isset($_db_props[$prop[$_mls_field['system_name']]])) {
        // It's a currently-existing node, so load it.
        // The third param tells node_load() to reset the loaded node cache,
        // reducing memory usage.
        $node = node_load($_db_props[$prop[$_mls_field['system_name']]]['nid'], NULL, TRUE);
        $new = FALSE;
        $_counts['update']++;
      }
      else {
        // It's not in the database, so let's create a new node.
        $node = new stdClass();
        $node->type = 'pirets_prop';
        $node->title = $prop[$_mls_field['system_name']];
        $new = TRUE;
        $_counts['new']++;
      }
      // Now edit the node.
      $loc = array(
        'street' => array(),
      );
      foreach ($context['results']['ask_fields'] as $field) {
        // Convert the value if necessary.
        if ($_fields[$field]['data_type'] === 'Date') {
          // Some day, we may properly use Date API to create these dates/times.
          // Today is not that day. Since the Date field assumes submitted time-
          // stamps are in GMT, account for that.
          $value = preg_match('/^(\d{4})-(\d{2})-(\d{2})$/', $prop[$field], $matches) ? gmmktime(0, 0, 0, $matches[2], $matches[3], $matches[1]) - variable_get('date_default_timezone', 0)  : NULL;
        }
        elseif ($_fields[$field]['data_type'] === 'DateTime') {
          $value = preg_match('/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2})$/', $prop[$field], $matches) ? gmmktime($matches[4], $matches[5], $matches[6], $matches[2], $matches[3], $matches[1]) - variable_get('date_default_timezone', 0) : NULL;
        }
        elseif ($_fields[$field]['lup_multi']) {
          $value = explode(',', $prop[$field]);
        }
        // Interealty, the piece of garbage, likes to send Ints with commas.
        // Thanks again, Interalty. http://drupal.org/node/834790
        elseif ($_fields[$field]['data_type'] === 'Int' || $_fields[$field]['data_type'] === 'Decimal') {
          $value = str_replace(',', '', $prop[$field]);
        }
        else {
          $value = $prop[$field];
        }
        $node_child = $_fields[$field]['cck'];
        if (is_array($value)) {
          $cck_value = array();
          foreach ($value as $val) {
            $cck_value[] = array('value' => $val);
          }
          $node->{$node_child} = $cck_value;
        }
        else {
          $node->{$node_child} = array(
            array(
              'value' => $value,
            ),
          );
        }
        // Should we use this for the body field?
        if ($_fields[$field]['correlation'] === 'body') {
          $node->body = $value;
        }
        elseif ($_fields[$field]['correlation'] === 'pic_count' && $_file) {
          $node->pirets_images = array();
          // Attach the placeholder image
          // First, can we create the requisite subdirectories?
          // The "level1" directory is built from a subset of the MLS ID, and
          // prevents us from creating more directories in $img_dir than some
          // filesystems can handle. http://drupal.org/node/842986
          $level1 = $img_dir . '/' . substr($prop[$_mls_field['system_name']], 0, 4);
          if (!is_dir($level1) && !mkdir($level1, 0755)) {
            watchdog('pirets', 'Directory @dir doesn\'t exist and couldn\'t be created (probably a permissions problem).', array('@dir' => $level1), WATCHDOG_ERROR);
          }
          else {
            $prop_dir = "{$level1}/{$prop[$_mls_field['system_name']]}";
            if (!is_dir($prop_dir) && !mkdir($prop_dir, 0755)) {
              watchdog('pirets', 'Directory @dir doesn\'t exist and couldn\'t be created (probably a permissions problem).', array('@dir' => $prop_dir), WATCHDOG_ERROR);
            }
            else {
              for ($index = 1; $index <= $value; $index++) {
                // Save the placeholder for each image attached to this listing.
                $fname = "{$prop[$_mls_field['system_name']]}-{$index}.jpeg";
                $dest = "{$prop_dir}/{$fname}";
                // Since file_copy stupidly doesn't let us specify the destination
                // path explicitly, stopping us from copying a file so it has a
                // different filename than the source, we do this workaround.
                $_file->filename = $fname;
                $fclone = clone $_file;
                if (file_copy($fclone, $prop_dir, FILE_EXISTS_REPLACE)) {
                  $_file->filepath = $dest;
                  drupal_write_record('files', $_file);
                  $_placeholder['fid'] = $_file->fid;
                  $_placeholder['filename'] = $fname;
                  $_placeholder['filepath'] = $dest;
                  $node->pirets_images[] = $_placeholder;
                }
                else {
                  watchdog('pirets', 'File @file couldn\'t be saved (probably a permissions problem).', array('@file' => $dest), WATCHDOG_ERROR);
                }
              }
            }
          }
        }
        elseif (strpos($_fields[$field]['correlation'], 'loc_') === 0) {
          if (preg_match('/^loc_street_(\d+)$/', $_fields[$field]['correlation'], $matches)) {
            if ($value !== '') {
              $loc['street'][intval($matches[1])] = $value;
            }
          }
          else {
            $loc[$_fields[$field]['correlation']] = $value;
          }
        }
      }
      if (variable_get('pirets_use_loc', FALSE)) {
        // Build values for Location field
        $node->pirets_loc = array(
          array(),
        );
        foreach (array('postal_code', 'province', 'city') as $field) {
          $loc_field = 'loc_' . $field;
          if (isset($loc[$loc_field])) {
            $node->pirets_loc[0][$field] = $loc[$loc_field];
          }
        }
        if (count($loc['street'])) {
          // Make sure street parts are in the proper order
          // http://drupal.org/node/862148
          ksort($loc['street']);
          $node->pirets_loc[0]['street'] = implode(' ', $loc['street']);
        }
      }
      $node->pirets_class = array(
        array(
          'value' => $context['results']['class'],
        ),
      );
      $node->pirets_crc32 = array(
        array(
          'value' => $prop['crc32'],
        ),
      );
      $node = node_submit($node);
      $node->revision = TRUE;
      node_save($node);
      if ($new) {
        watchdog('pirets', 'Creating node @nid (@title). MLS ID: @mls', array('@nid' => $node->nid, '@title' => $node->title, '@mls' => $prop[$_mls_field['system_name']]));
      }
      else {
        watchdog('pirets', 'Updating node @nid (@title). MLS ID: @mls', array('@nid' => $node->nid, '@title' => $node->title, '@mls' => $prop[$_mls_field['system_name']]));
      }
      // Clear the $_SESSION['messages'] to keep it from taking up a bunch of
      // memory
      unset($_SESSION['messages']);
    }
    // Else, leave the node alone
    else {
      $_counts['keep']++;
    }
    $context['finished'] = ++$_counts['checked'] / $_counts['total'];
    if (++$_index >= $chunk_count) {
      // Prepare for next chunk;
      $_index = 0;
      // Remove this chunk from cache;
      cache_clear_all($chunk_name, 'cache');
      $context['sandbox']['chunk_idx']++;
    }
  } // End of for loop
  $context['message'] = t('Processed @index of @count (@new created, @update updated) in class @class.', array('@index' => $_counts['checked'], '@count' => $_counts['total'], '@new' => $_counts['new'], '@update' => $_counts['update'], '@class' => $context['results']['class']));
}

/**
 * Delete old listings. Batch callback.
 */
function pirets_class_update_batch_del(&$context) {
  if (!count($context['sandbox'])) {
    if ($context['results']['error']) {
      $finished = 1;
      return;
    }
    // Both of these arrays contain MLS numbers.
    $context['sandbox']['to_delete'] = array_values(array_diff(array_keys($context['results']['db_props']), $context['results']['in_rets']));
    $context['sandbox']['to_delete_count'] = count($context['sandbox']['to_delete']);
    if ($context['sandbox']['to_delete_count'] === 0) {
      return;
    }
    $context['sandbox']['index'] = 0;
  }
  // Reacquire the semaphore.
  lock_acquire('pirets_fetch');
  $context['finished'] = 0;
  for ($x = 0; $x < 25 && $context['finished'] < 1; $x++) {
    $node = node_load($context['results']['db_props'][$context['sandbox']['to_delete'][$context['sandbox']['index']]]['nid'], NULL, TRUE);
    node_delete($node->nid);
    unset($_SESSION['messages']);
    $context['finished'] = ++$context['sandbox']['index'] / $context['sandbox']['to_delete_count'];
  }
  $context['results']['counts'][$context['results']['class']]['del'] = $context['sandbox']['index'];
  $context['message'] = t('Deleted @index of @count in class @class.', array('@index' => $context['sandbox']['index'], '@count' => $context['sandbox']['to_delete_count'], '@class' => $context['results']['class']));
}

/**
 * Display class update batch result to user. Batch callback.
 */
function pirets_class_update_batch_finished($success, $results, $operations) {
  // Release the semaphore.
  lock_release('pirets_fetch');
  if (count($results) && $results['counts'][$results['class']]['checked'] >= $results['counts'][$results['class']]['total']) {
    $classes = variable_get('pirets_classes', array());
    $classes[$results['class']]['last_update'] = time();
    variable_set('pirets_classes', $classes);
    if (variable_get('cron_semaphore', FALSE)) {
      watchdog('pirets', 'Class update for @class completed. @new created, @update updated, @del deleted, @keep unchanged.', array('@class' => $results['class'], '@new' => $results['counts'][$results['class']]['new'], '@update' => $results['counts'][$results['class']]['update'], '@del' => $results['counts'][$results['class']]['del'], '@keep' => $results['counts'][$results['class']]['keep']));
    }
    else {
      if ($results['error']) {
        drupal_set_message(t('Class update for @class did not finish due to a problem when connecting to the RETS server. See the <a href="!log">recent log entries page</a> for more information.', array('@class' => $results['class'], '!log' => url('admin/reports/dblog'))), 'error');
      }
      else {
        drupal_set_message(t('Class update for @class completed. @new created, @update updated, @del deleted, @keep unchanged.', array('@class' => $results['class'], '@new' => $results['counts'][$results['class']]['new'], '@update' => $results['counts'][$results['class']]['update'], '@del' => $results['counts'][$results['class']]['del'], '@keep' => $results['counts'][$results['class']]['keep'])));
      }
    }
  }
  else {
    drupal_set_message(t('It appears that class updating failed. If this was due to a PHP memory error, try increasing PHP&rsquo;s memory allocation (<em>memory_limit</em> value). If that is not an option, try decreasing the <em>Record chunk size</em> value in the <em>Advanced settings</em> section of the <a href="!slimits">Search limits</a> configuration page. If this was due to a PHP timeout error, try increasing PHP&rsquo;s execution time (<em>max_execution_time</em> value). Update the class again to continue fetching listings for it.', array('!slimits' => url('admin/settings/pirets/fields/limits'))));
  }
}

function pirets_all_update_batch_finished($success, $results, $operations) {
  // Release the semaphore.
  lock_release('pirets_fetch');
  foreach (pirets_classes_fetch() as $short => $class) {
    // This is dumb, but it will have to do for now
    if (isset($results['counts'][$short])) {
      $results['class'] = $short;
      pirets_class_update_batch_finished($success, $results, $operations);
    }
  }
}

/**
 * Delete property listing nodes. Batch callback.
 */
function pirets_flush_batch($nids, $class, &$context) {
  if (!count($context['sandbox'])) {
    $context['sandbox']['count'] = count($nids);
    if ($context['sandbox']['count'] === 0) {
      return;
    }
    $context['sandbox']['index'] = 0;
    $context['sandbox']['nids'] = $nids;
    $context['results']['class'] = $class;
  }

  // $context['finished'] is set to 1 at the onset of each call to a batch
  // function, apparently. Set it to zero so that the for loop below will
  // properly run its first run.
  $context['finished'] = 0;

  for ($x = 0; $x < 25 && $context['finished'] < 1; $x++) {
    $node = node_load($context['sandbox']['nids'][$context['sandbox']['index']], NULL, TRUE);
    node_delete($node->nid);
    unset($_SESSION['messages']);
    $context['finished'] = ++$context['sandbox']['index'] / $context['sandbox']['count'];
  }
  $context['message'] = t('Deleted @index of @count.', array('@index' => $context['sandbox']['index'], '@count' => $context['sandbox']['count']));
}

/**
 * Display result to the user. Batch callback.
 */
function pirets_flush_batch_finished($success, $results, $operations) {
  // Set the last_update times for the relative classes back to 'never'
  $classes = variable_get('pirets_classes', array());
  if ($results['class'] === NULL) {
    foreach ($classes as &$class) {
      $class['last_update'] = 0;
    }
  }
  else {
    $classes[$results['class']]['last_update'] = 0;
  }
  variable_set('pirets_classes', $classes);
  drupal_set_message('The property listings have been deleted.');
}

/**
 * Add and remove CCK fields to the pirets_prop content type. Batch callback.
 */
function pirets_fields_update_batch($form_values, &$context) {
  if (!count($context['sandbox'])) {
    $context['sandbox']['fields'] = array_values(pirets_fields_fetch());
    $context['sandbox']['form_values'] = $form_values;
    $context['sandbox']['index'] = 0;
    $context['sandbox']['count'] = count($context['sandbox']['fields']);
    $context['sandbox']['weight'] = 20;
    $context['results']['counts'] = array(
      'added' => 0,
      'removed' => 0,
    );
  }
  $context['finished'] = 0;
  module_load_include('inc', 'content', 'includes/content.crud');

  // Do this in batches of fifty
  for ($x = 0; $x < 50 && $context['finished'] < 1; $x++) {
    $field = &$context['sandbox']['fields'][$context['sandbox']['index']];
    // If the field is checked on the form, but we're not currently using it…
    if ($context['sandbox']['form_values'][$field['system_name']] && $field['cck'] === '') {
      $cck_field = array(
        // Truncate the field name to 32 characters, as CCK can't handle field
        // names longer than that. We're using the last 25 chars of the field's
        // RETS name because, on many RETS servers, the last characters of a
        // field's name seem to be more likely to be distinct than the first 25.
        // However, this has the side effect of possibly making the field name
        // more confusing for themers.
        // (25 + strlen('pirets_') === 32)
        'field_name' => 'pirets_' . substr($field['system_name'], -25),
        // A possible other approach if that's not distinct enough…?
        // 'field_name' => strlen($field['system_name']) <= 25 ? $field['system_name'] : crc32($field['system_name']),
        'type_name' => 'pirets_prop',
        'label' => $field['long_name'],
        'weight' => $weight += 10,
        'group' => 'group_pirets',
      );
      // What type of field are we creating?
      if ($field['lookup'] !== '') {
        // Lookup fields should always be text fields.
        $lookups = pirets_lookups_fetch($field['lookup'], TRUE);
        $values = array();
        $sort_by = array();
        foreach ($lookups as $lup) {
          $plain = check_plain($lup['long_value']);
          $sort_by[] = $plain;
          $values[] = "{$lup['value']}|{$lup['long_value']}";
        }
        // Alphabetize values
        array_multisort($sort_by, $values);
        $cck_field['type'] = 'text';
        $cck_field['max_length'] = $field['max_len'];
        $cck_field['widget_type'] = 'optionwidgets_select';
        $cck_field['allowed_values'] = implode("\n", $values);
        $cck_field['multiple'] = $field['lup_multi'];
      }
      else {
        if (in_array($field['data_type'], array('Boolean', 'Tiny', 'Small', 'Int', 'Long')) || ($field['data_type'] === 'Decimal' && $field['prec'] == 0)) {
          $cck_field['type'] = 'number_integer';
          $cck_field['widget_type'] = 'number';
        }
        elseif ($field['data_type'] === 'Decimal') {
          $cck_field['type'] = 'number_decimal';
          $cck_field['widget_type'] = 'number';
          // Note that RETS and this CCK field are using the term "precision"
          // differently
          $cck_field['precision'] = $field['max_len'];
          $cck_field['scale'] = $field['prec'];
        }
        elseif (in_array($field['data_type'], array('Date', 'DateTime'))) {
          $cck_field['type'] = 'datestamp';
          $cck_field['widget_type'] = 'date_text';
          $cck_field['widget_settings'] = array(
            'input_format' => 'm/d/Y - H:i:s',
          ); // @todo: Format this according to Date module defaults?
          $cck_field['year_range'] = '-0:+1';
          $cck_field['granularity'] = array(
            'year' => 'year',
            'month' => 'month',
            'day' => 'day',
            'hour' => 'hour',
            'minute' => 'minute',
            'second' => 'second',
          );
        }
        else {
          // Assume all others are Character
          $cck_field['type'] = 'text';
          $cck_field['max_length'] = $field['max_len'];
          $cck_field['widget_type'] = $field['max_len'] > 127 ? 'text_textarea' : 'text_textfield';
        }
      }

      // The FALSE param here (and in content_field_instance_delete() below)
      // stops CCK from rebuilding caches. We'll do that ourselves in
      // pirets_fields_update_batch_finished().
      // http://drupal.org/node/729722
      content_field_instance_create($cck_field, FALSE);
      if (module_exists('fieldgroup')) {
        fieldgroup_update_fields($cck_field);
      }
      $record = array(
        'system_name' => $field['system_name'],
        'cck' => $cck_field['field_name'],
      );
      drupal_set_message(t('Creating field %label (@id).', array('%label' => $cck_field['label'], '@id' => $field['system_name'])));
      drupal_write_record('pirets_fields', $record, 'system_name');
      $context['results']['counts']['added']++;
    }
    // Else if the field is unchecked on the form but we ARE currently using it…
    elseif (!$context['sandbox']['form_values'][$field['system_name']] && $field['cck'] !== '') {
      $label = db_result(db_query('SELECT label FROM {content_node_field_instance} WHERE field_name = "%s" && type_name = "%s"', 'pirets_' . $field['system_name'], 'pirets_prop'));
      drupal_set_message(t('Removing field %label (@id).', array('%label' => $label, '@id' => $field['system_name'])));
      content_field_instance_delete($field['cck'], 'pirets_prop', FALSE);
      // If that field was the image count field, delete the image field too.
      if ($field['correlation'] === 'pic_count') {
        drupal_set_message(t('Removing <em>Images</em> field.'));
        content_field_instance_delete('pirets_images', 'pirets_prop', FALSE);
      }
      $record = array(
        'system_name' => $field['system_name'],
        'cck' => '',
      );
      drupal_write_record('pirets_fields', $record, 'system_name');
      $context['results']['counts']['removed']++;
    }
    $context['finished'] = ++$context['sandbox']['index'] / $context['sandbox']['count'];
  }
  $context['message'] = t('Processed @index of @count.', array('@index' => $context['sandbox']['index'], '@count' => $context['sandbox']['count']));
}

/**
 * Display result to the user. Batch callback.
 */
function pirets_fields_update_batch_finished($success, $results, $operations) {
  cache_clear_all('pirets_fields', 'cache');
  cache_clear_all('pirets_fields_active', 'cache');
  // Clear/rebuild those aforementioned caches
  content_clear_type_cache(TRUE);
  menu_rebuild();

  drupal_set_message(t('%added CCK fields added; %removed CCK fields removed.', array('%added' => $results['counts']['added'], '%removed' => $results['counts']['removed'])));
}
